const mongoose = require('mongoose')
const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()

// Const Key MongoDB
const port = 3000
const username = "Cremsis1710"
const password = "%40Laura1710"
const dbName = "SubStream"


// Attivatori delle fn da noi richieste
app.use(express.static("public"))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.set("view engine", "ejs")


// Colegamneto a MongoDB
mongoose.connect(`mongodb+srv://${username}:${password}@cluster0.hbx7b.mongodb.net/${dbName}?retryWrites=true&w=majority`, {useNewUrlParser: true}, () => {
    console.log(`Colegamento con mongo db effetuato`)
})


// Attivazione di express
app.listen(port, () => {
    console.log(`Acesso alla ${port} effetuato`)
})


// Rotte
// HOME
// app.get("/", (req, res) => {res.render('home')}) 
// PROFILE
// app.get("/profile", (req, res) => {res.render('profile')})
// ADDSUBSCRIPTION
// app.get("/addSubscription", (req, res) => {res.render('addSubscription')})
// ORDERLIST
// app.get("/orderList", (req, res) => {res.render('orderList')})
const homeController = require('./controllers/homeController')
const profileController = require('./controllers/profileController')
const addSubController = require('./controllers/addSubController')
const orderListController = require('./controllers/orderListController')
const registerPageController = require('./controllers/registerPageController')
const registerActionController = require('./controllers/registerActionController')
const loginPageController = require('./controllers/loginPageController')
const loginActionController = require('./controllers/loginActionController')

app.get("/", homeController)
app.get("/profile", profileController)
app.get("/addSubscription", addSubController)
app.get("/orderList", orderListController)
app.get("/auth/register", registerPageController)
app.post("/auth/register", registerActionController)
app.get("/auth/login", loginPageController)
app.post("/auth/login", loginActionController)