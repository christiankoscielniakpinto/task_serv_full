const User = require("../models/users")

module.exports = (req, res) => {
    
    let utente = {
        username: req.body.inputUser,
        password: req.body.inputPassword
    }

    if(!utente.username || !utente.password){
        res.render('error', {
            details: "Errore inserisci i campi obligatori"
        })

    }else{
        User.findOne({
            username: utente.username
        }, (err, userFound) => {
            if(!err && userFound){

            }else{
                res.render('error', {
                    details: "Errore utente non trovato"
                })
            }
        })
    }
}