const User = require("../models/users")

module.exports = (req, res) => {
    let utente = {
        username: req.body.inputUser,
        password: req.body.inputPassword,
        role: req.body.inputRole
    }

    User.create(utente, (err, userCreated) => {
        if(!err && userCreated){
            res.redirect('/auth/login')
        }else{
            res.render('error', {
                details: err._message
            })
        }
    })

}